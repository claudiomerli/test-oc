FROM maven as builder
WORKDIR /opt/app
COPY src .
COPY pom.xml .
COPY mvnw* .

RUN mvn clean package -D skipTests

FROM openjdk:8-alpine
WORKDIR /opt/app
COPY --from=builder /opt/app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/opt/app/app.jar"]
