package it.solving.testspringmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpringmvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestSpringmvcApplication.class, args);
    }

}
