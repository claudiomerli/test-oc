package it.solving.testspringmvc;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
@RequestMapping(value = "/test")
public class CommonController {

    @GetMapping
    public ResponseEntity<String> getHostname() throws UnknownHostException {
        return ResponseEntity.ok(InetAddress.getLocalHost().getHostName());
    }

}
